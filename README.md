# RANDOM ART #

This is a simple *Java application* for making **Random Art** (pictures generated with elementary functions: sin(x), cos(x), constants,… ).

User can choose intensity (=expression length) of the pattern and colour (black&white, sepia-blue,…).

It is also possible to save picture that you like as a png file.

![randomart.png](https://bitbucket.org/repo/KAKkp8/images/2517892466-randomart.png)

### There are several files in this repository: ###
* RandomArt_Example.png is an example of a picture generated with this application.

* RandomArt_Application.jar is a jar file (I recommend this for using this  application. Download it and make your own Random Art.).

* .src (folder with .java files) is for those who are interested in the code.



                             Have fun and be artistic! :)