import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;


@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame implements ActionListener {

	private JButton gumb;

	private JButton gumb2;
	
    private JComboBox<String> comboBox;
    
    private JComboBox<String> comboBox2;

	private RandomSlika slika;
	
	public static boolean blackWhite = false;
	
	public static boolean sepia = false;
	
	public static boolean aestheticallyPleasing = false;

	public static int intensity = 2; //2=intense,1=medium,0=boring

	
	public GlavnoOkno() {
		super(); //poklicemo konstruktor od nadrazreda, da podeduje vse "lastnosti"
		setTitle("RandomArt"); //nastavimo naslov
		slika = new RandomSlika(512, 512);	
		
		//	NASTAVIMO LAYOUT MANAGER
		this.setLayout(new GridBagLayout()); //razlozimo kako bodo elementi v oknu razporejeni
		
		// DODAMO SLIKO
		GridBagConstraints c = new GridBagConstraints(); // + naredimo nov objekt c (GridBag..) 
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 0;
		c.gridx = 0;
		c.weightx = 5.0;
		c.weighty = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		add(slika, c);
		
		//vse v zvezi z dodajanjem GUMBA za risanje
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 2;
		c.weighty = (1/10);
		gumb = new JButton("MAKE YOUR OWN ART"); //naredimo GUMB
		gumb.addActionListener(this); //objekt this je narocen na dogodke od gumba
		add(gumb, c);
		
		//vse v zvezi z dodajanjem GUMBA za shranjevanje
		c = new GridBagConstraints();
		c.gridx = 3;
		c.gridy = 1;
		c.weightx = 1;
		c.weighty = (1/10);
		gumb2 = new JButton("SAVE PICTURE");
		gumb2.addActionListener(this); 
		add(gumb2, c);
				
		
		//drop down menu za izbiranje barve
		c = new GridBagConstraints();
		String[] type = {"Color", "Black&white", "Sepia blue", "Aesthetically pleasing"};
        comboBox = new JComboBox<>(type);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = (1/10);
		comboBox.addActionListener(this); 
        add(comboBox, c);
        
      //drop down menu za izbiranje intenzivnosti vzorca
  		c = new GridBagConstraints();
  		String[] intensityType = {"Intense", "Somwhere in the middle", "Boring"};
          comboBox2 = new JComboBox<>(intensityType);
          c.gridx = 1;
          c.gridy = 1;
          c.weightx = 1;
          c.weighty = (1/10);
  		comboBox2.addActionListener(this); 
          add(comboBox2, c);
		
	}

	//metoda, ki sprozi dogodek, ko uporabnik pritisne na gumb
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == gumb){ //metoda getSource pove kater objekt je sprozil dogodek
				slika.narisi(); //sliki nastavimo sliko
		} 
		if (e.getSource() == gumb2){
				slika.shraniSliko(); //shranimo sliko
		}
		if (comboBox.getSelectedItem().toString().equals("Black&white")){
			blackWhite = true;
			sepia = false;
			aestheticallyPleasing = false;
		}
        else if (comboBox.getSelectedItem().toString().equals("Color")){
			blackWhite = false;
			sepia = false;
			aestheticallyPleasing = false;
        }
        else if (comboBox.getSelectedItem().toString().equals("Sepia blue")){
			blackWhite = false;
			sepia = true;
			aestheticallyPleasing = false;
        }
        else if (comboBox.getSelectedItem().toString().equals("Aesthetically pleasing")){
			blackWhite = false;
			sepia = false;
			aestheticallyPleasing = true;

		}
		if (comboBox2.getSelectedItem().toString().equals("Intense")){
			intensity = 2;
		}
        else if (comboBox2.getSelectedItem().toString().equals("Somwhere in the middle")){
			intensity = 1;
		}
        else if (comboBox2.getSelectedItem().toString().equals("Boring")){
			intensity = 0;

        }
	}
	}
	

	

	

