import java.util.List;
import java.util.ArrayList;
import java.util.Random;

abstract class Izraz {
	public static List<Izraz> seznamIzrazov = new ArrayList<Izraz>();

	// abstraktna metoda za vrednost
	public abstract double vrednost(double x, double y);

	
	// metoda, ki vrne nakljucni izraz
	public static Izraz nakljucniIzraz(int velikost) {
		if (velikost <= 1) {
			// nakljucno izberemo enega od izrazov in ga naredimo
			Random random1 = new Random();
			int k = random1.nextInt(3) + 1; // stevilo izbir je 3
			if (k == 1) {
				return new X(velikost);
			} else if (k == 2) {
				return new Y(velikost);
			} else {
				return new Konstanta(velikost);
			}
		} else {
			// nakljucno izberemo enega od drugih izrazov in ga naredimo
			Random random = new Random();
			int k = random.nextInt(4) + 1; // stevilo izbir je 4
			if (k == 1) {
				return new Plus(velikost);
			} else if (k == 2) {
				return new Sin(velikost);
			} else if (k == 3) {
				return new Times(velikost);
			} else {
				return new Cos(velikost);
			}
		}
	}

	// podrazred X
	static class X extends Izraz {
		public X(int velikost) {
		}

		// metoda vrednost
		public double vrednost(double x, double y) {
			return x;
		}
	}

	// podrazred Y
	static class Y extends Izraz {
		public Y(int velikost) {
		}

		public double vrednost(double x, double y) {
			return y;
		}
	}

	// podrazred konstanta
	static class Konstanta extends Izraz {
		private double v;

		public Konstanta(int velikost) {
			Random random = new Random();
			//izberemo nakljucno stevilo med -1 in 1
			double rand = random.nextDouble();
			double scaled = rand * 2; // 2 = (1-(-1))
			this.v = scaled + (-1); // pristejemo se spodnjo mejo

		}

		public double vrednost(double x, double y) {
			return this.v;
		}
	}

	// podrazred PLUS
	static class Plus extends Izraz {
		private Izraz arg1;
		private Izraz arg2;

		public Plus(int velikost) {
			Random randomPlus = new Random();
			int velikost1 = randomPlus.nextInt(velikost) + 1;
			int velikost2 = velikost - 1 - velikost1;
			this.arg1 = Izraz.nakljucniIzraz(velikost1);
			this.arg2 = Izraz.nakljucniIzraz(velikost2);
		}

		public double vrednost(double x, double y) {
			return 0.5 * (arg1.vrednost(x, y) + arg2.vrednost(x, y));
		}
	}

	// podrazred TIMES
	static class Times extends Izraz {
		private Izraz arg1;
		private Izraz arg2;

		public Times(int velikost) {
			Random randomTimes = new Random();
			int velikost1 = randomTimes.nextInt(velikost) + 1;
			int velikost2 = velikost - 1 - velikost1;

			this.arg1 = Izraz.nakljucniIzraz(velikost1);
			this.arg2 = Izraz.nakljucniIzraz(velikost2);
		}

		public double vrednost(double x, double y) {
			return (arg1.vrednost(x, y) * arg2.vrednost(x, y));
		}
	}

	// podrazred SINUS
	static class Sin extends Izraz {
		private Izraz arg;
		private double freq;

		public Sin(int velikost) {
			this.arg = Izraz.nakljucniIzraz(velikost - 1);
			Random randomSin = new Random();
			this.freq = randomSin.nextInt(10) + 1;
		}

		public double vrednost(double x, double y) {
			double vrednostSin = Math.sin(freq * arg.vrednost(x, y));
			return vrednostSin;
		}
	}

	// podrazred COSINUS
	static class Cos extends Izraz {
		private Izraz arg;
		private double freq;

		public Cos(int velikost) {
			this.arg = Izraz.nakljucniIzraz(velikost - 1);
			Random randomCos = new Random();
			this.freq = randomCos.nextInt(10) + 1;
		}

		public double vrednost(double x, double y) {
			double vrednostCos = Math.sin(freq * arg.vrednost(x, y));
			return vrednostCos;
		}
	}

}
