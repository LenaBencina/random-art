import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class RandomSlika extends JPanel { // SlikaPanel je podclass Jpanel, to
											// je kot Canvas v tkinterju

	private BufferedImage slika; // slika kot objekt
	
	// KONSTRUKTOR
	public RandomSlika(int width, int height) {
		super();
		this.slika = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

	}

	
	// Metoda, ki bo izracunala vrednost za vsako točko
	public void izracunajSliko() {
		Random random = new Random();
		int velikost;
		//uporabnik izbere intenzivnost in s tem dolzino izraza
		if (GlavnoOkno.intensity == 2) {
			velikost = random.nextInt(50) + 45;

		} else if (GlavnoOkno.intensity == 1) {
			velikost = random.nextInt(15) + 10;

		} else {
			velikost = random.nextInt(3) + 2;

		}

		Izraz izraz1 = Izraz.nakljucniIzraz(velikost);
		Izraz izraz2 = Izraz.nakljucniIzraz(velikost);
		Izraz izraz3 = Izraz.nakljucniIzraz(velikost);


		// s tema zankama gremo po vseh tockah
		for (int x = 0; x < slika.getWidth(); x++) {
			double xDouble = (double) (x / (256.0) - 1); // pretvorimo x na interval [-1,1]
			if (x % 100 == 0) {
				repaint();
			}
			for (int y = 0; y < slika.getHeight(); y++) {

				double yDouble = (double) (y / (256.0) - 1); //pretvorimo y na interval [-1,1]
				double r = izraz1.vrednost(xDouble, yDouble); //shranimo vrednost izraza v komponente RGB
				double g = izraz2.vrednost(xDouble, yDouble);
				double b = izraz3.vrednost(xDouble, yDouble);

				int rInt = (int) ((r + 1) * 255 / 2); //pretvorimo komponente na interval [0, 255]
				int gInt = (int) ((g + 1) * 255 / 2);
				int bInt = (int) ((b + 1) * 255 / 2);

				int average;
				Color barva;
				//uporabnik izbira barvne "odtenke"
				if (GlavnoOkno.blackWhite) {
					average = (rInt + gInt + bInt) / 3;
					rInt = average;
					gInt = average;
					bInt = average;
				} else if (GlavnoOkno.sepia) {
					int sepiaDepth = 20;
					int sepiaIntensity = 30;
					average = (rInt + gInt + bInt) / 3;
					rInt = average + (sepiaDepth * 2);
					gInt = average + sepiaDepth;

					if (rInt > 255) {
						rInt = 255;
					}
					if (gInt > 255) {
						gInt = 255;
					}
					if (bInt > 255) {
						bInt = 255;
					}

					bInt -= sepiaIntensity;

					if (bInt < 0) {
						bInt = 0;
					}
					if (bInt > 255) {
						bInt = 255;
					}
				} else if (GlavnoOkno.aestheticallyPleasing) {
					Random random2 = new Random();
					int r2 = random2.nextInt(256);
					int g2 = random2.nextInt(256);
					int b2 = random2.nextInt(256);

					rInt = (r2 + rInt) / 2;
					gInt = (g2 + gInt) / 2;
					bInt = (b2 + bInt) / 2;
				}
				barva = new Color(rInt, gInt, bInt);
				int rgb = barva.getRGB();
				slika.setRGB(x, y, rgb); //nastavimo sliki RGB

			}
		}
	}


	// vzporedno vlakno
	public void narisi() {
		Runnable slikar = new Runnable() {
			public void run() {
				izracunajSliko();
				repaint();
			}
		};
		new Thread(slikar).start();
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(slika.getWidth(), slika.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int w = slika.getWidth();
		int h = slika.getHeight();
		int x = (this.getWidth() - w) / 2;
		int y = (this.getHeight() - h) / 2;
		g.drawImage(slika, x, y, w, h, Color.BLACK, null);

	}
	
	//funkcija, ki shrani sliko v png file
	public void shraniSliko() {
		try {
			SimpleDateFormat datum = new SimpleDateFormat("ddMMyy-hhmmss.SSS"); // dobimo datum
			Random random = new Random();
			File f = new File(String.format("%s.%s", datum.format(new Date()),
					random.nextInt(9) + ".png"));
			ImageIO.write(slika, "png", f);
			System.out.println("Writing complete.");
		} catch (IOException e) {
			System.out.println("Error: " + e);
		}
	}
}
